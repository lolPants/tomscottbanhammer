﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using WebSocketSharp;

[Serializable]
public class StringEvent : UnityEvent<string> { }

public class WebSocketComponent : MonoBehaviour {

    public string defaultURL = "ws://localhost:3000/";

    public TextMeshPro textMesh;

    private int retries;
    public int maxRetries = 8;
    private bool shouldReconnect;

    public UnityEvent OnOpen;
    public UnityEvent OnClose;
    public StringEvent OnMessage;

    [HideInInspector]
    public WebSocket socket;

	void Start () {
        string url;
        string filePath = Path.Combine(Application.dataPath, "url.txt");

        try
        {
            url = File.ReadAllText(filePath);
        }
        catch
        {
            url = defaultURL;
            File.WriteAllText(filePath, defaultURL);
        }

        socket = new WebSocket(url);

        socket.OnOpen += (sender, e) => OnOpen.Invoke();
        socket.OnClose += (sender, e) => OnClose.Invoke();
        socket.OnMessage += (sender, e) => OnMessage.Invoke(e.Data);

        socket.OnOpen += (sender, e) =>
        {
            retries = 0;
            textMesh.text = "Connected!";
        };

        socket.OnClose += (sender, e) =>
        {
            if (e.Code == (ushort)CloseStatusCode.Away)
                return;

            shouldReconnect = true;
        };
    }

    private void FixedUpdate()
    {
        if (shouldReconnect)
        {
            shouldReconnect = false;
            StartCoroutine("Reconnect");
        }
    }

    IEnumerator Reconnect ()
    {
        if (retries < maxRetries)
        {
            textMesh.text = $"Reconnecting... ({retries + 1} of {maxRetries})";

            retries++;
            yield return new WaitForSeconds(5f);
            socket.Connect();
        }
        else
        {
            textMesh.text = "Connection failed!";
        }
    }
}
