﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Managers : MonoBehaviour {

	private static Managers instance;

	public static IPublicNodeManager Node
	{
		get { return instance.node; }
	}
	NodeManager node = null;

	public static IPublicStageManager Stage
	{
		get { return instance.stage; }
	}
	StageManager stage = null;

	public static bool Debug = true;

	void Awake()
	{
		instance = this;
		node = GetComponentInChildren<NodeManager>();
		stage = GetComponentInChildren<StageManager>();
	}
}
