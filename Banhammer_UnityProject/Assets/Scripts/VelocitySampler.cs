﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocitySampler : MonoBehaviour {

	Vector3 prevPos;
	public float Velocity
	{
		get { return velocity; }
	}
	float velocity;

	int numberOfSamples = 8;
	float[] samples;
	int currentSampleIndex = 0;
	
	void Start ()
	{
		samples = new float[numberOfSamples];
		prevPos = transform.position;
	}

	public void Sample ()
	{
		samples[currentSampleIndex] = Mathf.Abs((transform.position - prevPos).magnitude);
		currentSampleIndex += 1;
		currentSampleIndex %= numberOfSamples;
		
		prevPos = transform.position;

		velocity = 0;
		for (int i = 0; i < samples.Length; i++)
		{
			velocity += samples[i];
		}
		velocity /= samples.Length;
	}
}
