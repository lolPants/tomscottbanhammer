﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerPivot : MonoBehaviour {
	
	[SerializeField]
	Transform target;

	[SerializeField]
	Transform GripHand;

	void LateUpdate ()
	{
		if (target != null)
		{
			// point the hammer from the grip hand, to the pivot hand
			transform.LookAt(target, Vector3.up);

			// pivot the hammer with the user's wrist
			if (GripHand != null)
			{
				transform.rotation *= Quaternion.Euler(0.0f, 0.0f, -GripHand.transform.rotation.eulerAngles.z);
			}
		}
	}
}
