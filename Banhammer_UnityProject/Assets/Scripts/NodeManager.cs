﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using WebSocketSharp;

[RequireComponent(typeof(WebSocketComponent))]
public class NodeManager : MonoBehaviour, IPublicNodeManager {

    WebSocketComponent wsComponent;

    private void Start()
    {
        wsComponent = FindObjectOfType<WebSocketComponent>();

        wsComponent.socket.OnOpen += (sender, e) => OnConnected();
        wsComponent.socket.OnClose += (sender, e) => OnDisconnected();
        wsComponent.socket.OnMessage += (sender, e) => OnMessage(e.Data);

        wsComponent.socket.Connect();
    }

    void OnConnected()
    {
        if (Managers.Debug)
            Debug.Log("Connected to Node server...");
    }

    void OnDisconnected()
    {
        if (Managers.Debug)
            Debug.Log("...Disconnected from Node server");
    }

    void OnMessage(string data)
    {
        var json = JSON.Parse(data);

        string e = json["event"];
        string username = json["username"];

        if (e == "stageUser")
            Managers.Stage.StageUsername(username);
    }

    public void Ban(string _username, int _duration)
    {

        if (wsComponent.socket.ReadyState == WebSocketState.Open)
        {
            string payload = string.Format(@"{{""event"": ""ban"", ""username"":""{0}"", ""duration"":""{1}""}}", _username, _duration);

            if (Managers.Debug)
                Debug.Log("Banning " + payload);

            wsComponent.socket.Send(payload);
        }
        else if (Managers.Debug)
        {
            Debug.Log("Banning of " + _username + " failed, not connected to Node server!");
        }
    }
}

public interface IPublicNodeManager {
	void Ban(string _username, int _duration);
}
