﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StageManager : MonoBehaviour, IPublicStageManager {

	[SerializeField]
	GameObject plinth;

	[SerializeField]
	GameObject avatarPrefab;

	public Banhammer Banhammer {
		get { return banhammer; }
	}

	[SerializeField]
	Banhammer banhammer;

	public Avatar CurrentAvatar {
		get { return currentAvatar; }
		set { currentAvatar = value; }
	}
	Avatar currentAvatar;

	public string CurrentUsername {
		get { return currentUsername; }
		set { currentUsername = value; }
	}
	string currentUsername = null;

	bool is_staged = false;

    public Queue<string> _pending = new Queue<string>();
    private object _asyncLock = new object();
    public TextMeshPro othertext;

    public void StageUsername (string _username)
    {
        lock (_asyncLock)
        {
            _pending.Enqueue(_username);
        }
    }

	public void ResetAndStage(string _username)
	{
		if (Managers.Debug)
            Debug.Log("Staging : " + _username);

        currentUsername = _username;

		if (currentAvatar != null)
            currentAvatar.Remove();

        if (othertext != null)
            othertext.text = _username;

        SpawnAvatar(_username);
		is_staged = true;
	}

	void SpawnAvatar(string _username)
	{
		currentAvatar = GameObject.Instantiate(avatarPrefab, plinth.transform.position, Quaternion.identity).GetComponent<Avatar>();
		currentAvatar.Init(_username);
	}

	public void TriggerBan(int _duration)
	{
		if (currentUsername != null && is_staged)
		{
			Managers.Node.Ban(currentUsername, _duration);
			is_staged = false;

            if (othertext != null)
                othertext.text = "";
        }
	}

    public void Update()
    {
        if (_pending.Count == 0) return;

        lock (_asyncLock)
        {
            foreach (string username in _pending)
                ResetAndStage(username);

            _pending.Clear();
        }
    }

}

public interface IPublicStageManager {
	void ResetAndStage(string _username);
	void TriggerBan(int _duration);
    void StageUsername(string _username);
	string CurrentUsername { get; set; }
	Avatar CurrentAvatar { get; set; }
	Banhammer Banhammer { get; }
}
