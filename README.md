# TomScottBanhammer

Functional mixed-reality Banhammer for Tom Scott's YouTube channel!

Maintained order for NerdCubed's stream on 30/08/18 : www.youtube.com/watch?v=nyD7FMwhwxc

These assets are free for you to learn from and play with :)
I'm afraid the code is a bit messy, as most of it was written quickly for a very specific purpose.
I have checked, but if anyone spots an included third-party file (i.e. a node module) where the license prohibits this form of re-distribution, please let me know!


## How do I?
Firstly, you'll need to have Node.js installed (nodejs.org)

This project interfaces with Twitch using 'tmi js'. Rather than work directly with the Twitch API, I opted to remote control a moderator account.
To set this up with your own account / channel, you'll need to edit the "Node\banhammerApp.js" file:
```javascript
const channelName = "myChannel" 	// set this to the channel you want to moderate
const modUsername = "myMod"   		// set this to the moderator user you want to remote control
const modPassword = "myPasswordOAuth"   // generate an OAuth by logging into the chosen account, then go to twitchapps.com/tmi
```

Then, run banhammerapp.js using node from the command line.

The "Banhammer_UnityProject" folder is a Unity 2018.2.5f1+ project. 
Inside you'll find a working version of the Banhammer scene, and some example environment assets.
Hit play, and in the banhammerapp.js command line you'll see: "TWITCH READY" followed by "UNITY READY".

Now, simply type any username, hit enter, and it should spawn an avatar within the Unity scene, ready for smashing :)

## How did you?
The models were created in Blender using the built-in modelling and sculpting tools. 
For some models, such as the fractured avatar, I also used Houdini and Instant Meshes to generate the fragments and low-res meshes respectively.

Normal maps were baked in Blender (float buffer, saved as 16 bit tiff), then brought into Knald to generate AO and curvature maps.
These were combined in Krita with some basic colour tinting and contrast, to generate stylised textures with appropriate edge wear.

The system was developed in Unity* and Node JS, linking the two with respective Socket IO packages.

*mainly just for the easy interfacing with Steam VR when we shot with the HTC Vive mixed reality configuration.