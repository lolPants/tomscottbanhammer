const WebSocket = require('ws')
const tmi = require('tmi.js')
const dotenv = require('dotenv')
const readline = require('readline')

// Load Environment Variables
dotenv.config()
const { CHANNEL_NAME, MOD_USERNAME, MOD_TOKEN } = process.env

// Configure the client, then connect
const options = {
  options: {
    debug: false,
  },
  connection: {
    cluster: 'aws',
    reconnect: true,
  },
  identity: {
    username: MOD_USERNAME,
    password: MOD_TOKEN,
  },
  channels: [CHANNEL_NAME],
}

// Connect to Twitch
const client = new tmi.Client(options)
client.connect()

client.on('disconnected', () => {
  console.log('\n - TWITCH DISCONNECTED - \n')
})

client.on('connected', () => {
  console.log('\n - TWITCH READY - \n')
})

// Interactive console
const interactiveConsole = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
})

interactiveConsole.on('line', input => stageUser(input))

// Connection to Unity app
const wss = new WebSocket.Server({ port: 3000 })
/**
 * @type {WebSocket}
 */
let socket = null

wss.on('connection', _socket => {
  socket = _socket
  console.log('\n - UNITY READY - \n')

  socket.on('message', data => {
    try {
      const { event, username, duration } = JSON.parse(data)
      if (event !== 'ban') return undefined

      ban(username, duration)
    } catch (err) {
      // Silently fail
    }
  })
})

/**
 * Sends a username along to the game
 * @param {string} username User to stage
 */
const stageUser = username => {
  if (socket !== null && client.readyState() === 'OPEN') {
    const payload = JSON.stringify({ event: 'stageUser', username })
    socket.send(payload)
    console.log(`${username} is at the mercy of the Banhammer`)
  } else {
    console.log(`Failed to stage ${username}. Unity socket is not connected`)
  }
}

/**
 * Bans a user from Twitch Chat
 * @param {string} username User to ban
 * @param {number} duration Duration in seconds
 */
const ban = async (username, duration) => {
  if (client.readyState() !== 'OPEN') return undefined

  try {
    await client.timeout(CHANNEL_NAME, username, duration, '')
    console.log(`${username} has been smitten with a strength of ${duration}!!!`)
  } catch (err) {
    console.log(`Error : ${err}`)
  }
}
